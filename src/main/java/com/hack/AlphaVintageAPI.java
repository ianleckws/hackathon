package com.hack;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;
public class AlphaVintageAPI {
    public static void main(String[] args) {
        String apifilename= "D:\\citibank\\New_training\\week3\\project\\hackathon\\hackathon\\src\\main\\resources\\apikey.txt";
;        AlphaVintageAPI alphaapi=null;
        try {
            alphaapi = new AlphaVintageAPI(apifilename);
        }
        catch (FileNotFoundException f){
            System.out.println("File not found !");
        }
        System.out.println( alphaapi.getCrypto("BTC","2021-08-15"));
    }

    private String apikey;
    public AlphaVintageAPI(String apifilename) throws FileNotFoundException {
        File apifile= new File(apifilename);
        BufferedReader br = new BufferedReader(new FileReader(apifile));
        try {
            apikey = br.readLine();
            System.out.println(apikey);
        }
        catch (IOException e){
            System.out.println("Failed to read the files ");
        }
        }

    public JSONObject getStock (String symbol){

        String baseurl ="https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=%s&interval=5min&apikey=%s";
        String apiurl= String.format(baseurl, symbol, this.apikey);

        try {
            JSONObject msg = JsonReader.readJsonFromUrl(apiurl);
            JSONObject data= msg.getJSONObject("Time Series (5min)");
            return data;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }
    public JSONObject getCrypto(String symbol, String date){
        String baseurl="https://www.alphavantage.co/query?function=DIGITAL_CURRENCY_DAILY&symbol=%s&market=CNY&apikey=%s";
        String apiurl= String.format(baseurl, symbol, this.apikey);
        try {
            JSONObject msg = JsonReader.readJsonFromUrl(apiurl);
            JSONObject data= msg.getJSONObject("Time Series (Digital Currency Daily)");
            JSONObject prices = data.getJSONObject(date);
            return prices;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null ;
    }

}

class JsonReader {

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
        InputStream is = new URL(url).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONObject json = new JSONObject(jsonText);
            return json;
        } finally {
            is.close();
        }
    }
}