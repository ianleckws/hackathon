package com.hack.service;

import com.hack.entity.User;

import java.util.List;

public interface UserService {
    void save(User user);

    User findByUsername(String username);

    User findByEmail(String email);

    User findById(int id);

    Iterable<User> findAll();

    List<Integer> getAllIds();

    void deleteById(int deleted_user);
}
