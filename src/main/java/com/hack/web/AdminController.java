package com.hack.web;

import com.hack.entity.User;
import com.hack.repository.UserRepository;
import com.hack.service.SecurityService;
import com.hack.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Slf4j
@Controller
public class AdminController {

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserValidator userValidator;

    @Autowired
    private UserService userService;

    @PreAuthorize("hasAuthority('Admin')")
    @GetMapping("/admin")
    public String handleAdminLogin(Model model, String error, String logout) {
        if (securityService.isAuthenticated()) {
            return handleAdminHome(model);
        }

        if (error != null)
            model.addAttribute("error", "Your username and/or password are invalid.");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");

        return "adminlogin";
    }

    @GetMapping("/admin-console")
    public String handleAdminHome (Model model){
        model.addAttribute("Users", userService.findAll());
        model.addAttribute("createUser",new User());
        model.addAttribute("updateUser",new User());
        model.addAttribute("userIDs", userService.getAllIds());
        return "userinformation";
    }

    @PostMapping("/admin-console/add")
    public String handleCreate (Model model, @ModelAttribute User createdUser){
        userService.save(createdUser);
        return handleAdminHome(model);
    }

    @PostMapping("/admin-console/update")
    public String handleUpdate (Model model, @RequestParam(value="customer_ID") int user_ID, @ModelAttribute User updatedUser){
        log.info("updatedUser = {}", updatedUser);
        User existingUser = userService.findById(user_ID);
        existingUser.setEmail(updatedUser.getEmail());
        existingUser.setUsername(updatedUser.getUsername());
        userService.save(existingUser);
        return handleAdminHome(model);
    }

    @PostMapping("/admin-console/delete")
    public String handleDelete (Model model, @RequestParam(value="delete_ID") int deleted_user){
        userService.deleteById(deleted_user);
        return handleAdminHome(model);
    }

}
