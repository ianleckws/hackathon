package com.hack.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name = "Users")
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;

    private String name;

    private String username;

    private String email;

    @Transient
    private String emailConfirm;

    private String password;

    @Transient
    private String passwordConfirm;

    public enum Role{User,Admin}
    private Role role;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Ticker> watchlist;

    public User() {
        // Empty no-arg constructor.
    }

    public User(int id, String email, String name, String password, String username, Role role) {
        this.id = id;
        this.email = email;
        this.name = name;
        this.password = password;
        this.username = username;
        this.role = role;
    }

    @Override
    public String toString(){
        return String.format("[%d] %s, %s, %s", id, name, username, email);
    }
}