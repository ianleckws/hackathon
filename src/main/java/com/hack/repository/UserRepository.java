package com.hack.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.hack.entity.User;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Integer> {

//    @Query(value="select * from user u where u.email = :email", nativeQuery = true)
    User findByEmail(String email);
    User findByUsername(String username);
    User findById(int id);
//    @Query(value="select * from user u where u.firstname = :firstname and u.lastname = :lastname", nativeQuery = true)
//    User findByName(@Param("firstname") String firstname, @Param("lastname") String lastname);

    @Query(value="select id from users", nativeQuery=true)
    List<Integer> getAllIds();

}