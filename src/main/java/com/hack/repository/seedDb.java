package com.hack.repository;

import com.hack.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Slf4j
@Component
public class seedDb {
    @Autowired
    UserRepository repository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @PostConstruct
    public void init() {
        log.info("Initialized postConstruct");
        User newUser = new User(0, "admin@admin.com", "admin", bCryptPasswordEncoder.encode("adminPassword"), "adminUser", User.Role.Admin);
        log.info("Admin created = {}", newUser);
        repository.save(newUser);
    }
}
