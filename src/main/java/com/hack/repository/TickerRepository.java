package com.hack.repository;

import com.hack.entity.Ticker;
import org.springframework.data.repository.CrudRepository;

public interface TickerRepository extends CrudRepository<Ticker, Integer> {

}
