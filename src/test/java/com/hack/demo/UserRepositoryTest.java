package com.hack.demo;

import com.hack.entity.User;
import com.hack.repository.UserRepository;
import com.hack.service.UserService;
import com.hack.service.UserServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.junit.jupiter.api.Assertions.assertEquals;

//@Slf4j
public class UserRepositoryTest {
//
//    @Autowired
//    private UserRepository mockRepo;
//    @MockBean
//    private UserService mockService;
//
//    @Autowired
//    private BCryptPasswordEncoder bCryptPasswordEncoder;
//
//    @Test
//    public void testFindUserByName() throws Exception {
//        String email = "andy@neueda.com";
//        User andy = new User(1, email, "andy", bCryptPasswordEncoder.encode("swanseaFan"), "num1Swansea", User.Role.User);
//        Mockito.when(mockRepo.findByUsername("num1Swansea").getEmail()).thenReturn(email);
//        assertEquals(email, mockService.findByUsername("num1Swansea").getEmail());
//    }

//    @Test
//    public void testGetAllIds(){
//        mockRepo.save(new User(2, "nicole@neueda.com", "andy", bCryptPasswordEncoder.encode("swanseaFan"), "nicolelee", User.Role.User));
//        mockRepo.save(new User(3, "daini@neueda.com", "andy", bCryptPasswordEncoder.encode("swanseaFan"), "ianleck", User.Role.User));
//        mockRepo.save(new User(4, "ian@neueda.com", "andy", bCryptPasswordEncoder.encode("swanseaFan"), "dainiwang", User.Role.User));
//        mockRepo.save(new User(5, "zhibo@neueda.com", "andy", bCryptPasswordEncoder.encode("swanseaFan"), "zhiboh", User.Role.User));
//        assertEquals(5,mockRepo.getAllIds().size());
//    }
//
//    @Test
//    public void testExistsById(){
//        assertEquals(true, mockRepo.existsById(1));
//    }

}