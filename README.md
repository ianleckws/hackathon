What we managed to get working for sprint 4:
- Application Bug fixes from Sprint 3 (Ian)
- Spring Security Permissions (Ian)
- Containerization (Daini)
- Implementation of Openshift (Daini)
- Lambda function to call REST API (Nicole/Zhibo)
- PoC for AWS Cognito (Daini)
- PoC for AWS RDS (Daini)
---
What we didn't manage to get working:
- Tests
---
Note that Sprint 3 is on release branch, Sprint 4 is on Master
---